require_relative 'quantity'

class Operation

	def initialize(buttonArray)
		@buttonArray = buttonArray
	end

	def add(i)
		i = i
		#puts @buttonArray[@i].getter

		if addCheck(i)
			number = @buttonArray[i].getter + 1
			puts number
			@buttonArray[i].setter(number)
			
		end

	end

	def subtract(l)

		l=l

		if addCheck(l)

			if l != 0

				if subtractCheck(0)
					@buttonArray[0].setter(@buttonArray[0].getter - 1)
				elsif l != 1 && subtractCheck(2)
					@buttonArray[1].setter(@buttonArray[1].getter - 1)
				elsif l != 2 && subtractCheck(1)
					@buttonArray[2].setter(@buttonArray[2].getter - 1)
				end
			elsif (@buttonArray[1].getter) >= (@buttonArray[2].getter)
				@buttonArray[1].setter(@buttonArray[1].getter - 1)
			else
				@buttonArray[2].setter(@buttonArray[2].getter - 1)
			end
		end
	end

	def addCheck(i)
		i = i
		check = 0
		#puts "addCheck"
		for k in 0..(@buttonArray.length - 1)
			if (k != i) and (@buttonArray[k].getter == 0)
				check = check + 1 
			end
		end
		
		if check == (@buttonArray.length - 1)
			return false
		else
			return true
		end
	end

	def subtractCheck(j)

		j=j

		if @buttonArray[j].getter == 0
			return false
		else
			return true
		end
	end
end

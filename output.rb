
class Output

	def dataToBeShown(buttonArray)
		
		@buttonArray = buttonArray

		outputForButton()

	end

	def outputForButton
		puts "Button - 1 : Element - #{@buttonArray[0].getter} : Press - 1"
		puts "Button - 2 : Element - #{@buttonArray[1].getter} : Press - 2"
		puts "Button - 3 : Element - #{@buttonArray[2].getter} : Press - 3"
		puts "For exit Press - 4"
	end
end